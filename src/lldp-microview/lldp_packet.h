/*******************************************************************************
 * Projekt:         Programování síťové služby: Sniffer CDP a LLDP
 * Jméno:           Radim
 * Příjmení:        Loskot
 * Login autora:    xlosko01
 * E-mail:          xlosko01(at)stud.fit.vutbr.cz
 * Popis:           Hlavičkový soubor daklarující třídu LLDP paketu.
 *
 ******************************************************************************/

/**
 * @file lldp_packet.h
 *
 * @brief Header file which declares class of LLDP packet.
 * @author Radim Loskot xlosko01(at)stud.fit.vutbr.cz
 */

#ifndef LLDP_PACKET_H
#define LLDP_PACKET_H

    static const uint16_t ETHER_TYPE = 0xCC88;       /**< Ethernet type for LLDP packet rev endianness */
    static const byte TL_SIZE = 2;               /**< Size of type-value items */
    static const uint16_t MAX_VALUE_SIZE = 256;      /**< Max size of value item */
    static const byte ORG_SPEC_LEN = 3;      /**< Max size of value item */
    static const byte ieee[ORG_SPEC_LEN] = {0x00, 0x80, 0xc2};
    static const byte tele[ORG_SPEC_LEN] = {0x00, 0x12, 0xbb};

    typedef struct lldp_str {
        char* str;
        byte len;
    } lldp_str;
    typedef struct lldp_mac {
        byte m1; byte m2; byte m3;
        byte m4; byte m5; byte m6;
    } lldp_mac;
    typedef struct lldp_port {
        byte subtype;
        lldp_str id;
        lldp_str description;
    } lldp_port;
    typedef struct lldp_caps {
        uint8_t caps;
        uint8_t enabled;
    } lldp_caps;
    typedef struct lldp_vlan {
        uint16_t id;
        lldp_str name;
    } lldp_vlan;
    typedef struct lldp {
        lldp_str name;
        lldp_str description;
        lldp_port port;
        lldp_caps capabilities;
        lldp_vlan vlan;
        lldp_vlan vlan2;
    } lldp;

    /**
      * Enumeration of all TLV types of LLDP packet with correspond value in packet
      */
    enum TLV_types {
        endOfLLPDU             = 0,
        chassisID              = 1,
        portID                 = 2,
        timeToLive             = 3,
        portDescription        = 4,
        systemName             = 5,
        systemDescription      = 6,
        systemCapabilities     = 7,
        managementAddress      = 8,
        orgSpecific            = 127
    };

    enum pID_types {
        interfaceAlias     = 1,
        portComponent      = 2,
        macAddress         = 3,
        networkAddress     = 4,
        interfaceName      = 5,
        agentCircuitID     = 6,
        locallyAssigned    = 7
    };

    enum cID_types {
        cchassisComponent   = 1,
        cinterfaceAlias     = 2,
        cportComponent      = 3,
        cmacAddress         = 4,
        cnetworkAddress     = 5,
        cinterfaceName      = 6,
        cagentCircuitID     = 7,
        clocallyAssigned    = 8
    };

    enum capabilities {
        other      = 0x01,
        repeater   = 0x02,
        bridge     = 0x04,
        wlanAP     = 0x08,
        router     = 0x10,
        telephone  = 0x20,
        DOCSIS     = 0x40,
        station    = 0x80,
    };

    /**
      * Class of chassis ID value.
      * Value is variant and depends on subtype.
      * @see TLV
      */
/*
    enum managementAddressSubtypes {
        IPv4                = 0x01,
        IPv6                = 0x02,
        NSAP                = 0x03,
        HDLC                = 0x04,
        BBN_1822            = 0x05,
        all802              = 0x06,
        E_163               = 0x07,
        E_164               = 0x08,
        F_69                = 0x09,
        X_121               = 0x0A,
        IPX                 = 0x0B,
        appleTalk           = 0x0C,
        decnetIV            = 0x0D,
        banyanVines         = 0x0E,
        E_164_NSAP          = 0x0F,
        DNS                 = 0x10
        // and other
    };
    enum interfaceNumberingSubtype {
        unknown             = 0x01,
        ifIndex             = 0x02,
        systemPortNumber    = 0x03
    };
    const static uint8_t END_OF_LLDPDU[2];
*/
#endif
