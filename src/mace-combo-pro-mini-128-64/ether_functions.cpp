/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <Arduino.h>
#include <EtherCard.h>
#include "ether_functions.h"

uint16_t readUShort(int position, int fromBit, int count) {
    uint8_t *dw = const_cast<uint8_t *>((uint8_t*)ether.buffer + position);
    fromBit %= 8;
    count %= 16;        // output unsigned short has only 16 bits

    return (uint16_t)
    // Bitwise OR of shifted first uint8_t left and second uint8_t shifted rigth + folding left
    ((((((uint8_t)(dw[0] << fromBit) | (uint8_t)(dw[1] >> (8 - fromBit)))    << 8) |
    // Bitwise OR of shifted second uint8_t left and third uint8_t shifted rigth
    ((uint8_t)(dw[1] << fromBit) | (uint8_t)(dw[2] >> (8 - fromBit)))))
    // Skipping bits on the right
    >> (16 - count));
}

uint16_t readUShort(int position) {
    return *(uint16_t *)((uint8_t*)ether.buffer + position);
}

uint8_t readUChar(int position, int fromBit, int count) {
    uint8_t *db = const_cast<uint8_t *>((uint8_t*)ether.buffer + position);
    fromBit %= 8;
    count %= 8;

    return (uint8_t)(((uint8_t)(*db << fromBit) | (uint8_t)(db[1] >> (8 - fromBit))) >> (8 - count));
}

uint8_t readUChar(int position) {
    return *(uint8_t *)((uint8_t*)ether.buffer + position);
}

uint8_t toCidr(uint8_t ipbytes[4]) {
	uint8_t netmask_cidr = 0;

	for (int i=0; i<4; i++) {
		switch(ipbytes[i]) {
			case 0x80:
				netmask_cidr+=1;
				break;

			case 0xC0:
				netmask_cidr+=2;
				break;

			case 0xE0:
				netmask_cidr+=3;
				break;

			case 0xF0:
				netmask_cidr+=4;
				break;

			case 0xF8:
				netmask_cidr+=5;
				break;

			case 0xFC:
				netmask_cidr+=6;
				break;

			case 0xFE:
				netmask_cidr+=7;
				break;

			case 0xFF:
				netmask_cidr+=8;
				break;

			default:
				return netmask_cidr;
				break;
		}
	}

	return netmask_cidr;
}
