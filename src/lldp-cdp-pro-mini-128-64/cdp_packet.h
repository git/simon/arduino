/* 
    LLDP Reader - Arduino Pro Mini / Pro Micro + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

static const byte CDP_TLV_H_SIZE = 4;

static byte cdp_broadcast_mac[] = {0x01, 0x00, 0x0c, 0xcc, 0xcc, 0xcc};
static byte llc_for_cdp[] = {0xaa, 0xaa, 0x03, 0x00, 0x00, 0x0c, 0x20, 0x00};

enum CDP_TLV_types {
    cdp_deviceID               = 0x0001,
    cdp_addresses              = 0x0002,
    cdp_portID                 = 0x0003,
    cdp_capabilities           = 0x0004,
    cdp_softwareVersion        = 0x0005,
    cdp_platform               = 0x0006,
    cdp_protocolHello          = 0x0008,
    cdp_VTPmanagementDomain    = 0x0009,
    cdp_nativeVLAN             = 0x000a,
    cdp_duplex                 = 0x000b,
    cdp_voipVLAN               = 0x000e,
    cdp_trustBitmap            = 0x0012,
    cdp_untrustedPortCoS       = 0x0013,
    cdp_managementAddress      = 0x0016
};

