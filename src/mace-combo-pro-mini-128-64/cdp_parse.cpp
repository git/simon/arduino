/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EtherCard.h>
#include "lldp_parse.h"
#include "cdp_parse.h"
#include "ether_functions.h"

static byte cdp_broadcast_mac[] = {0x01, 0x00, 0x0c, 0xcc, 0xcc, 0xcc};
static byte llc_for_cdp[] = {0xaa, 0xaa, 0x03, 0x00, 0x00, 0x0c, 0x20, 0x00};

uint8_t checkCDP(uint16_t len) {
    uint8_t isCDP = true;
    uint8_t bufidx = 0;
    if (len < 30) return false;

    for (uint8_t i = 0; i < sizeof(cdp_broadcast_mac) && isCDP; ++i, ++bufidx) {
        isCDP = (readUChar(bufidx) == cdp_broadcast_mac[i]);
    }
    bufidx += sizeof(cdp_broadcast_mac);
    bufidx += sizeof(uint16_t);
    for (uint8_t i = 0; i < sizeof(llc_for_cdp) && isCDP; ++i, ++bufidx) {
        isCDP = (readUChar(bufidx) == llc_for_cdp[i]);
    }
    bufidx += sizeof(llc_for_cdp);

    return isCDP;
}

void parseCDP(uint16_t len, lldp *packet) {
    uint16_t length = 0;
    uint16_t position = 26;
    
    while (position + CDP_TLV_H_SIZE < len) {
        uint8_t* tlv_data = ether.buffer + position;
        uint16_t type = (((uint16_t)*tlv_data)<<8) | (uint16_t)*(tlv_data + 1);
        length = (((uint16_t)*(tlv_data + 2))<<8) | (uint16_t)*(tlv_data + 3);

        // test whether length of TLV exceeds the end of packet
        if (position + length >= len) {
            break;
        }

        if (length < CDP_TLV_H_SIZE + 1) {
            position += CDP_TLV_H_SIZE;
            continue;
        }

        tlv_data = ether.buffer + position + CDP_TLV_H_SIZE;
        switch (type) {
            case CDP_TLV_types::cdp_portID:
                packet->port.id.str = (char*)tlv_data;
                packet->port.id.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_deviceID:
                packet->name.str = (char*)tlv_data;
                packet->name.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_platform:
                packet->description.str = (char*)tlv_data;
                packet->description.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_nativeVLAN:
                packet->vlan.id = (((uint16_t)*tlv_data)<<8) | (uint16_t)*(tlv_data + 1);
                break;
            case CDP_TLV_types::cdp_voipVLAN:
                // skip data byte
                packet->vlan2.id = (((uint16_t)*(tlv_data + 1))<<8) | (uint16_t)*(tlv_data + 2);
                break;
            default:
                break;
        }
        position += length;
    }
}

