/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <EtherCard.h>
#include "lldp_parse.h"
#include "ether_functions.h"


char port_subtype(uint8_t subtype) {
    switch (subtype) {
        case pID_types::interfaceAlias:
            return 'A';
        case pID_types::portComponent:
            return 'P';
        case pID_types::macAddress:
            return 'M';
        case pID_types::networkAddress:
            return 'N';
        case pID_types::interfaceName:
            return 'I';
        case pID_types::agentCircuitID:
            return 'C';
        case pID_types::locallyAssigned:
            return 'L';
    }
    return '?';
}

char* capstring(lldp_caps c, char* tlvbuffer) {
    int8_t i = 0;
    if (c.caps & capabilities::other)
        tlvbuffer[i++] = (c.enabled & capabilities::other)     ? 'O' : 'o';
    if (c.caps & capabilities::repeater)
        tlvbuffer[i++] = (c.enabled & capabilities::repeater)  ? 'P' : 'p';
    if (c.caps & capabilities::bridge)
        tlvbuffer[i++] = (c.enabled & capabilities::bridge)    ? 'B' : 'b';
    if (c.caps & capabilities::wlanAP)
        tlvbuffer[i++] = (c.enabled & capabilities::wlanAP)    ? 'W' : 'w';
    if (c.caps & capabilities::router)
        tlvbuffer[i++] = (c.enabled & capabilities::router)    ? 'R' : 'r';
    if (c.caps & capabilities::telephone)
        tlvbuffer[i++] = (c.enabled & capabilities::telephone) ? 'T' : 't';
    if (c.caps & capabilities::DOCSIS)
        tlvbuffer[i++] = (c.enabled & capabilities::DOCSIS)    ? 'D' : 'd';
    if (c.caps & capabilities::station)
        tlvbuffer[i++] = (c.enabled & capabilities::station)   ? 'S' : 's';
    tlvbuffer[i] = 0;
    return tlvbuffer;
}

void parseLLDP(uint16_t len, lldp *packet) {
    uint16_t length = 0;
    uint16_t position = 14;
    
    while (position + 1 < len) {
        TLV_types type = (TLV_types)readUChar(position, 0, 7);
        length = readUShort(position, 7, 9);

        // test whether length of TLV exceeds the end of packet
        if (position + TL_SIZE + length >= len) {
            break;
        }
        if (type == endOfLLPDU) {
            break;
        }

        if (length < 1) {
            position += TL_SIZE;
            continue;
        }

        uint8_t* tlv_data = ether.buffer + position + TL_SIZE;
        switch (type) {
            case TLV_types::chassisID:
                break;
            case TLV_types::portID:
                packet->port.subtype = *tlv_data;
                packet->port.id.str = (char*)tlv_data + 1;
                packet->port.id.len = length - 1;
                break;
            case TLV_types::timeToLive:
                break;
            case TLV_types::portDescription:
                packet->port.description.str = (char*)tlv_data;
                packet->port.description.len = length;
                break;
            case TLV_types::systemName:
                packet->name.str = (char*)tlv_data;
                packet->name.len = length;
                break;
            case TLV_types::systemDescription:
                packet->description.str = (char*)tlv_data;
                packet->description.len = length;
                break;
            case TLV_types::systemCapabilities:
                packet->capabilities.caps = *(tlv_data + 1);
                packet->capabilities.enabled = *(tlv_data + 1 + 2);
                break;
            case TLV_types::managementAddress:
                break;
            case TLV_types::orgSpecific: {
                uint8_t matchieee = true;
                uint8_t matchtele = true;
                for (uint8_t i = 0; i < ORG_SPEC_LEN; ++i) {
                    if (tlv_data[i] != ieee[i]) matchieee = false;
                    if (tlv_data[i] != tele[i]) matchtele = false;
                }
                uint8_t subtype = *(tlv_data + ORG_SPEC_LEN);
                lldp_vlan* vlan = &packet->vlan;
                if (matchtele) {
                    if (subtype != 0x02) break;
                    uint16_t vlanpos = position + TL_SIZE + ORG_SPEC_LEN + 2;

                    uint16_t vlanid = readUShort(vlanpos, 3, 12);
                    if (packet->vlan.id != vlanid && packet->vlan.id > 0)
                        vlan = &(packet->vlan2);
                    vlan->id = vlanid;
                }
                if (matchieee) {
                    if (subtype == 0x01 || subtype == 0x03) {
                        uint8_t* vlanptr = tlv_data + ORG_SPEC_LEN + 1;
                        uint16_t vlanid = (((uint16_t)*vlanptr)<<8) | (uint16_t)*(vlanptr + 1);
                        if (packet->vlan.id != vlanid && packet->vlan.id > 0)
                            vlan = &(packet->vlan2);
                        vlan->id = vlanid;
                        if (subtype == 0x03) {
                            vlan->name.len = *(vlanptr + 2);
                            vlan->name.str = (char*)vlanptr + 3;
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
        // move position to the start of next TLV
        position += length + TL_SIZE;
    }
}

