/* 
    LLDP + CDP Reader - Arduino Pro Mini / Pro Micro + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
    Diagnostics:
    No Mace in lower right corner = no Ethernet module communications.
    Mace moving side to side = packets
    Mace not moving = no packets
*/

#include "lcdgfx.h"
#include <SPI.h>
#include <Wire.h>
#include <EtherCard.h>
#include "scp_12x16.h"
#include "lldp_packet.h"
#include "cdp_packet.h"

uint8_t Ethernet::buffer[800];
static uint8_t mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
static uint8_t myip[] = { 192,168,102,142 };
#define ethpin 9

uint8_t mace[] = {
  0x00, 0x30, 0xF8, 0xE8, 0xC8, 0x1E, 0xFB, 0x1E, 0xC8, 0xE8, 0xF8, 0x30, 0x00, 
  0x00, 0x00, 0x00, 0x13, 0x3F, 0xEE, 0x6D, 0xAE, 0x3F, 0x13, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xDD, 0x66, 0xBB, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x30, 0x4D, 0xA6, 0x4B, 0x30, 0x00, 0x00, 0x00, 0x00, 
};

int8_t maceoffset = 0;
uint8_t macewrap = false;


class DisplaySSD1306_128x64_SPI_CursorGetters: public  DisplaySSD1306_128x64_SPI
{
  public:
    explicit DisplaySSD1306_128x64_SPI_CursorGetters(int8_t rstPin, const SPlatformSpiConfig &config = {-1, {-1}, -1, 0, -1, -1})
        : DisplaySSD1306_128x64_SPI(rstPin, config)
    {
    }
    uint8_t getTextCursorX()
    {
      return (uint8_t)m_cursorX;
    }
    uint8_t getTextCursorY()
    {
      return (uint8_t)m_cursorY;
    }
};


DisplaySSD1306_128x64_SPI_CursorGetters display(6,{-1, 8, 7, 0,-1,-1});

void setup() {
    display.setFixedFont(ssd1306xled_font6x8);
    display.begin();

    /* remove below two lines to flip display */
    display.getInterface().flipVertical(1);
    display.getInterface().flipHorizontal(1);

    display.clear();

    display.setFixedFont(Source_Code_Pro12x16);
    display.printFixed(0, 0, ">", STYLE_NORMAL); // L
    display.printFixed(0, 16, ">", STYLE_NORMAL); // L
    display.printFixed(0, 32, "?", STYLE_NORMAL); // D
    display.printFixed(0, 48, "@", STYLE_NORMAL); // P
    display.setFixedFont(ssd1306xled_font6x8);
    display.printFixed(12, 0, "ink", STYLE_NORMAL);
    display.printFixed(12, 16, "ayer", STYLE_NORMAL);
    display.printFixed(12, 32, "iscovery", STYLE_NORMAL);
    display.printFixed(12, 48, "rotocol", STYLE_NORMAL);
    display.printFixed(18, 56, " ~ Simon M", STYLE_NORMAL);

    if (ether.begin(sizeof Ethernet::buffer, mymac, ethpin) == 0) {
        display.printFixed(36, 0, "!ERR   !", STYLE_NORMAL);
        display.printFixed(36, 8, "!NO ETH!", STYLE_NORMAL);
        for(;;) {
            delay(100);
        }
    }

    display.drawBuffer1(114, 32, 13, 32, mace);

    ether.staticSetup(myip);
    ether.enablePromiscuous();
    ether.enableBroadcast();
    ether.enableMulticast();
}

uint16_t readUShort(int position, int fromBit, int count) {
    uint8_t *dw = const_cast<uint8_t *>((uint8_t*)ether.buffer + position);
    fromBit %= 8;
    count %= 16;        // output unsigned short has only 16 bits

    return (uint16_t)
    // Bitwise OR of shifted first uint8_t left and second uint8_t shifted rigth + folding left
    ((((((uint8_t)(dw[0] << fromBit) | (uint8_t)(dw[1] >> (8 - fromBit)))    << 8) |
    // Bitwise OR of shifted second uint8_t left and third uint8_t shifted rigth
    ((uint8_t)(dw[1] << fromBit) | (uint8_t)(dw[2] >> (8 - fromBit)))))
    // Skipping bits on the right
    >> (16 - count));
}

uint16_t readUShort(int position) {
    return *(uint16_t *)((uint8_t*)ether.buffer + position);
}

uint8_t readUChar(int position, int fromBit, int count) {
    uint8_t *db = const_cast<uint8_t *>((uint8_t*)ether.buffer + position);
    fromBit %= 8;
    count %= 8;

    return (uint8_t)(((uint8_t)(*db << fromBit) | (uint8_t)(db[1] >> (8 - fromBit))) >> (8 - count));
}

uint8_t readUChar(int position) {
    return *(uint8_t *)((uint8_t*)ether.buffer + position);
}

                                     
char port_subtype(uint8_t subtype) {
    switch (subtype) {
        case pID_types::interfaceAlias:
            return 'A';
        case pID_types::portComponent:
            return 'P';
        case pID_types::macAddress:
            return 'M';
        case pID_types::networkAddress:
            return 'N';
        case pID_types::interfaceName:
            return 'I';
        case pID_types::agentCircuitID:
            return 'C';
        case pID_types::locallyAssigned:
            return 'L';
    }
    return '?';
}

char tlvbuffer[MAX_VALUE_SIZE];
char* buf_lldp_str(lldp_str val, uint8_t len) {
    if (!len) len = val.len;
    for (uint16_t i = 0; i < len; ++i) {
        tlvbuffer[i] = val.str[i];
    } tlvbuffer[len] = '\0';
    return tlvbuffer;
}
char* buf_lldp_str(lldp_str val) {
    return buf_lldp_str(val, 0);
}
char* capstring(lldp_caps c) {
    int8_t i = 0;
    if (c.caps & capabilities::other)
        tlvbuffer[i++] = (c.enabled & capabilities::other)     ? 'O' : 'o';
    if (c.caps & capabilities::repeater)
        tlvbuffer[i++] = (c.enabled & capabilities::repeater)  ? 'P' : 'p';
    if (c.caps & capabilities::bridge)
        tlvbuffer[i++] = (c.enabled & capabilities::bridge)    ? 'B' : 'b';
    if (c.caps & capabilities::wlanAP)
        tlvbuffer[i++] = (c.enabled & capabilities::wlanAP)    ? 'W' : 'w';
    if (c.caps & capabilities::router)
        tlvbuffer[i++] = (c.enabled & capabilities::router)    ? 'R' : 'r';
    if (c.caps & capabilities::telephone)
        tlvbuffer[i++] = (c.enabled & capabilities::telephone) ? 'T' : 't';
    if (c.caps & capabilities::DOCSIS)
        tlvbuffer[i++] = (c.enabled & capabilities::DOCSIS)    ? 'D' : 'd';
    if (c.caps & capabilities::station)
        tlvbuffer[i++] = (c.enabled & capabilities::station)   ? 'S' : 's';
    tlvbuffer[i] = 0;
    return tlvbuffer;
}

size_t ssdwrite(uint8_t ch) {
    if (display.getTextCursorX() > (display.getTextCursorY() < 32 ? 128 : 112) - 6)
    {
        if (display.getTextCursorY() >= display.height() - 8)
        {
            display.setTextCursor(12, 0);
        }
        else
        {
            display.setTextCursor(12, display.getTextCursorY() + 8);
        }
        if ((char)ch != '\n')
        {
            display.print((char)ch);
        }
    }
    else
    {
        if (display.getTextCursorX() < 12)
        {
            display.setTextCursor(12, display.getTextCursorY());
        }
        display.print((char)ch);
    }
    return 1;
}

size_t ssdprint(const char ch[]) {
    size_t n = 0;
    while (*ch)
    {
        n += ssdwrite(*ch);
        ch++;
    }
    return n;
}
          
uint8_t ssdprintln(const char *ch) {
    return ssdprint(ch) + ssdprint("\n");
}

static lldp packet;

void parseLLDP(uint16_t len) {
    uint16_t length = 0;
    uint16_t position = 14;
    
    while (position + 1 < len) {
        TLV_types type = (TLV_types)readUChar(position, 0, 7);
        length = readUShort(position, 7, 9);

        // test whether length of TLV exceeds the end of packet
        if (position + TL_SIZE + length >= len) {
            break;
        }
        if (type == endOfLLPDU) {
            break;
        }

        if (length < 1) {
            position += TL_SIZE;
            continue;
        }

        uint8_t* tlv_data = ether.buffer + position + TL_SIZE;
        switch (type) {
            case TLV_types::chassisID:
                break;
            case TLV_types::portID:
                packet.port.subtype = *tlv_data;
                packet.port.id.str = (char*)tlv_data + 1;
                packet.port.id.len = length - 1;
                break;
            case TLV_types::timeToLive:
                break;
            case TLV_types::portDescription:
                packet.port.description.str = (char*)tlv_data;
                packet.port.description.len = length;
                break;
            case TLV_types::systemName:
                packet.name.str = (char*)tlv_data;
                packet.name.len = length;
                break;
            case TLV_types::systemDescription:
                packet.description.str = (char*)tlv_data;
                packet.description.len = length;
                break;
            case TLV_types::systemCapabilities:
                packet.capabilities.caps = *(tlv_data + 1);
                packet.capabilities.enabled = *(tlv_data + 1 + 2);
                break;
            case TLV_types::managementAddress:
                break;
            case TLV_types::orgSpecific: {
                uint8_t matchieee = true;
                uint8_t matchtele = true;
                for (uint8_t i = 0; i < ORG_SPEC_LEN; ++i) {
                    if (tlv_data[i] != ieee[i]) matchieee = false;
                    if (tlv_data[i] != tele[i]) matchtele = false;
                }
                uint8_t subtype = *(tlv_data + ORG_SPEC_LEN);
                lldp_vlan* vlan = &packet.vlan;
                if (matchtele) {
                    if (subtype != 0x02) break;
                    /*uint8_t apptype = *(tlv_data + ORG_SPEC_LEN + 1);*/
                    uint16_t vlanpos = position + TL_SIZE + ORG_SPEC_LEN + 2;
                    /*uint8_t tag = readUChar(vlanpos, 1, 1); // 1 or 0*/

                    uint16_t vlanid = readUShort(vlanpos, 3, 12);
                    if (packet.vlan.id != vlanid && packet.vlan.id > 0)
                        vlan = &packet.vlan2;
                    vlan->id = vlanid;
                }
                if (matchieee) {
                    if (subtype == 0x01 || subtype == 0x03) {
                        uint8_t* vlanptr = tlv_data + ORG_SPEC_LEN + 1;
                        uint16_t vlanid = (((uint16_t)*vlanptr)<<8) | (uint16_t)*(vlanptr + 1);
                        if (packet.vlan.id != vlanid && packet.vlan.id > 0)
                            vlan = &packet.vlan2;
                        vlan->id = vlanid;
                        if (subtype == 0x03) {
                            vlan->name.len = *(vlanptr + 2);
                            vlan->name.str = (char*)vlanptr + 3;
                        }
                    }
                }
                break;
            }
            default:
                break;
        }
        // move position to the start of next TLV
        position += length + TL_SIZE;
    }
}

void printLLDP() {
    // clear center, leave MACE and bitmaps on sides
    display.setColor(0x0000);
    display.fillRect(0,0,128,24);
    display.fillRect(12,0,112,64);
    display.setColor(0xFFFF);
    display.setFixedFont(Source_Code_Pro12x16);
    display.printFixed(0, 0, ">", STYLE_NORMAL);
    display.printFixed(0, 16, ">", STYLE_NORMAL);
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);

    ssdprint("Port `");
    if (atoi(buf_lldp_str(packet.port.id))) {
        ssdwrite(port_subtype(packet.port.subtype));
        ssdwrite(' ');
        ssdprintln(tlvbuffer);
    }
    if (packet.port.description.len > 0) {
        ssdprintln(buf_lldp_str(packet.port.description));
    }
    if (packet.name.len > 0) {
        ssdprint("Name `");
        ssdprintln(capstring(packet.capabilities));
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.vlan.id) {
        ssdprint("VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
        ssdprintln(buf_lldp_str(packet.vlan.name));
    }
    if (packet.vlan2.id) {
        ssdprint("VLA2 ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
        //ssdprintln(buf_lldp_str(packet.vlan2.name));
    }
}

uint8_t checkCDP(uint16_t len) {
    uint8_t isCDP = true;
    uint8_t bufidx = 0;
    if (len < 30) return false;

    for (uint8_t i = 0; i < sizeof(cdp_broadcast_mac) && isCDP; ++i, ++bufidx) {
        isCDP = (readUChar(bufidx) == cdp_broadcast_mac[i]);
    }
    bufidx += sizeof(cdp_broadcast_mac);
    bufidx += sizeof(uint16_t);
    for (uint8_t i = 0; i < sizeof(llc_for_cdp) && isCDP; ++i, ++bufidx) {
        isCDP = (readUChar(bufidx) == llc_for_cdp[i]);
    }
    bufidx += sizeof(llc_for_cdp);

    return isCDP;
}

void parseCDP(uint16_t len) {
    uint16_t length = 0;
    uint16_t position = 26;
    
    while (position + CDP_TLV_H_SIZE < len) {
        uint8_t* tlv_data = ether.buffer + position;
        uint16_t type = (((uint16_t)*tlv_data)<<8) | (uint16_t)*(tlv_data + 1);
        length = (((uint16_t)*(tlv_data + 2))<<8) | (uint16_t)*(tlv_data + 3);

        // test whether length of TLV exceeds the end of packet
        if (position + length >= len) {
            break;
        }

        if (length < CDP_TLV_H_SIZE + 1) {
            position += CDP_TLV_H_SIZE;
            continue;
        }

        tlv_data = ether.buffer + position + CDP_TLV_H_SIZE;
        switch (type) {
            case CDP_TLV_types::cdp_portID:
                packet.port.id.str = (char*)tlv_data;
                packet.port.id.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_deviceID:
                packet.name.str = (char*)tlv_data;
                packet.name.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_platform:
                packet.description.str = (char*)tlv_data;
                packet.description.len = length - CDP_TLV_H_SIZE;
                break;
            case CDP_TLV_types::cdp_nativeVLAN:
                packet.vlan.id = (((uint16_t)*tlv_data)<<8) | (uint16_t)*(tlv_data + 1);
                break;
            case CDP_TLV_types::cdp_voipVLAN:
                // skip data byte
                packet.vlan2.id = (((uint16_t)*(tlv_data + 1))<<8) | (uint16_t)*(tlv_data + 2);
                break;
            default:
                break;
        }
        position += length;
    }
}

void printCDP() {
    display.setColor(0x0000);
    display.fillRect(0,0,128,24);
    display.fillRect(12,0,112,64);
    display.setColor(0xFFFF);
    display.setFixedFont(Source_Code_Pro12x16);
    display.printFixed(0, 16, "C", STYLE_NORMAL);
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);

    ssdprint("Port: ");
    if (buf_lldp_str(packet.port.id)) {
        ssdprintln(tlvbuffer);
    }
    if (packet.name.len > 0) {
        ssdprint("Name: ");
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.description.len > 0) {
        ssdprint("Model: ");
        ssdprintln(buf_lldp_str(packet.description));
    }
    if (packet.vlan.id) {
        ssdprint("Native VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprintln(tlvbuffer);
    }
    if (packet.vlan2.id) {
        ssdprint("Voice VLAN ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
    }
}

unsigned long completed = 0;
void loop() {
    uint16_t len = ether.packetReceive();
    if (!len) return;

    if (completed + 30 < (uint16_t)millis()) {
        if (maceoffset < 2 && !macewrap) {
            ++maceoffset;
            display.drawBuffer1(114 + maceoffset, 32, 13, 32, mace);
        }
        else if (maceoffset > -2 && macewrap) {
            --maceoffset;
            display.drawBuffer1(114 + maceoffset, 32, 13, 32, mace);
        }
        else {
            macewrap = !macewrap;
            display.drawBuffer1(114 + maceoffset, 32, 13, 32, mace);
        }
        completed = (uint16_t)millis();
    }

    uint16_t result = ether.packetLoop(len);

    if (result == 1 && readUShort(12) == ETHER_TYPE) {
        // LLDP
        parseLLDP(len);
        printLLDP();
        completed = (uint16_t)millis();
        memset(&packet, 0, sizeof(lldp));
    }
    else if (result == 1 && checkCDP(len)) {
        // CDP
        parseCDP(len);
        printCDP();
        completed = (uint16_t)millis();
        memset(&packet, 0, sizeof(lldp));
    }
    else {
        // not LLDP or CDP
        return;
    }
}

