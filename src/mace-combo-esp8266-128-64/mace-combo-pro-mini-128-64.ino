/* 
    LLDP + CDP + MAC Address + DHCP Reader - ESP8266 + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
    Using Tool Menu:
    Connect pin 2 to ground using a button. This will rotate through the 
    available tools.

    Diagnostics:
    No Mace in lower right corner = no Ethernet module communications.
    Mace moving side to side = packets
    Mace not moving = no packets
*/

#include <SPI.h>
#include <Wire.h>
#include <EtherCard.h>
#include "SSD1306Spi.h"
#include "lldp_parse.h"
#include "cdp_parse.h"
#include "ether_functions.h"

/*
 * Ethernet parameters
 */
uint8_t Ethernet::buffer[800];
const uint8_t mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
const uint8_t myip[] = { 192,168,102,142 };
const char dns_domain[] = "molzy.com";
static byte broadcast[] = { 0xff,0xff,0xff,0xff,0xff,0xff };
#define ETH_PIN D0

char tlvbuffer[MAX_VALUE_SIZE];
static lldp packet;
uint8_t dhcp_result = 99;

/*
 * Tool selection menu
 */

// Interrupt-capable pin for menu navigation button
#define MENU_PIN D8

// Menu countdown (0-9)
#define MENU_SECONDS 5

enum tool {
    MENU      = 99,
    LLDP_CDP  = 0,
    MACE      = 1,
    DHCP      = 2,
};
tool tool_selected = tool::MENU;
tool menu_selected = tool::LLDP_CDP;
#define TOOL_COUNT 3
static byte menu_countdown = MENU_SECONDS;

const char *menuItems[] =
{
    "LLDP / CDP", // LLDP / CDP
    "MAC Address", // MACE
    "DHCP Client", // DHCP
};

// Menu state
/*LcdGfxMenu menu( menuItems, sizeof(menuItems) / sizeof(char *) , { {4, 0}, {112, 64} } );*/

/*
 * Mace bitmap
 * Big endian, horizontal bytes
 */
const uint8_t mace[] PROGMEM = {
0x20,0x00, 
0x70,0x00,  
0x50,0x00, 
0xFE,0x03,  
0x73,0x06, 
0x27,0x07, 
0xAE,0x03, 
0xAE,0x03,  
0xAC,0x01, 
0xDC,0x01,  
0xF8,0x00, 
0xF8,0x00,  
0x8C,0x01, 
0xF8,0x00, 
0x60,0x00, 
0x50,0x00,  
0x50,0x00, 
0x30,0x00,  
0x60,0x00, 
0x50,0x00,  
0x50,0x00, 
0x30,0x00, 
0x60,0x00, 
0x50,0x00,  
0x50,0x00, 
0x30,0x00,  
0x60,0x00, 
0x50,0x00,  
0x88,0x00, 
0xA8,0x00, 
0x50,0x00, 
0x20,0x00
};

int8_t maceoffset = 0;
uint8_t macewrap = false;

SSD1306Spi display(D2, D1, D3);
/*OLEDDisplayUi ui(&display);*/

// Text drawing functions
size_t ssdnewline();
size_t ssdwrite(uint8_t ch);
size_t ssdprint(const char ch[]);
size_t ssdprintln(const char *ch);

/*
 * Setup and ISR functions
 */
void ICACHE_RAM_ATTR menu_ISR();
void setup_menu();
void display_menu();
void setup_lldp();
void setup_mac();
void setup_dhcp();


void setup() {
    Serial.begin(115200);
    Serial.println("[*] We have HI!");

    pinMode(MENU_PIN, INPUT);
    attachInterrupt(digitalPinToInterrupt(MENU_PIN), menu_ISR, RISING);

    // remove below two lines to flip display 
    display.init();
    Serial.println("^_^ Display initialised.");
    /*display.flipScreenVertically();*/
    /*display.mirrorScreen();*/

    display.clear();

    Serial.println(":^o Checking ETH...");

    if (ether.begin(sizeof Ethernet::buffer, mymac, ETH_PIN) == 0) {
        display.setTextAlignment(TEXT_ALIGN_CENTER);
        display.drawString(0, 48, "!ERR!\n!NO  ETH!");
        for(;;) {
            delay(100);
        }
    }
    Serial.println(":^) Acquired ETH!");
    Serial.println("\n[~] Welcome to MACE\n[~] By Simon M");

    tool_selected = tool::LLDP_CDP;
    setup_lldp();

    Serial.print("[>] Selected Mode - ");
    Serial.println(menuItems[menu_selected]);
}

void setup_menu() {
    int x = 0; int y = 0;
    char countdown[2] = {0};

    display.clear();
    display.drawXbm(116 + x, 32 + y, 16, 32, mace);
    display.setFont(ArialMT_Plain_10);
    countdown[0] = menu_countdown + 0x30;
    display.drawString(120 + x, 6 + y, countdown);

    display_menu();
}

void display_menu() {
    int x = 0; int y = 0;
    display.setColor(BLACK);
    display.fillRect(0 + x, 24 + y, 114, 16);
    display.setColor(WHITE);
    display.setFont(ArialMT_Plain_16);
    display.drawString(0 + x, 24 + y, menuItems[menu_selected]);
    display.setFont(ArialMT_Plain_10);

    Serial.print("[>] Menu Item - ");
    Serial.println(menuItems[menu_selected]);

    display.display();
}


void setup_lldp() {
    int x = 0; int y = 0;
    ether.staticSetup(myip);
    ether.enablePromiscuous();
    ether.enableBroadcast();
    ether.enableMulticast();

    display.clear();

    display.setFont(ArialMT_Plain_16);
    display.drawString(0 + x,  0 + y, "L");
    display.drawString(0 + x, 16 + y, "L");
    display.drawString(0 + x, 32 + y, "D");
    display.drawString(0 + x, 48 + y, "P");
    display.setFont(ArialMT_Plain_10);
    display.drawString(12 + x,  0 + y, "ink");
    display.drawString(12 + x, 14 + y, "ayer");
    display.drawString(12 + x, 30 + y, "iscovery");
    display.drawString(12 + x, 46 + y, "rotocol");
    display.drawString(12 + x, 54 + y, " ~ Simon M");

    display.drawXbm(116 + x, 32 + y, 16, 32, mace);

    display.display();
}

void setup_mac() {
    int x = 0; int y = 0;
    ether.staticSetup(myip);
    ether.enablePromiscuous();
    ether.enableBroadcast();
    ether.enableMulticast();

    display.clear();

    display.setFont(ArialMT_Plain_16);
    display.drawString(0 + x,  0 + y, "M");
    display.drawString(0 + x, 16 + y, "A");
    display.drawString(0 + x, 32 + y, "C");
    display.drawString(0 + x, 48 + y, "E");
    display.setFont(ArialMT_Plain_10);
    display.drawString(12 + x,  0 + y, "ac");
    display.drawString(12 + x, 14 + y, "ddress");
    display.drawString(12 + x, 30 + y, "ompact");
    display.drawString(12 + x, 46 + y, "xtractor");
    display.drawString(12 + x, 54 + y, " ~ Simon M");

    display.drawXbm(116 + x, 32 + y, 16, 32, mace);

    display.display();
}

void setup_dhcp() {
    int x = 0; int y = 0;
    ether.enableBroadcast();
    ether.disablePromiscuous();
    ether.disableMulticast();

    display.clear();

    display.setFont(ArialMT_Plain_16);
    display.drawString(0 + x,  0 + y, "D");
    display.drawString(0 + x, 16 + y, "H");
    display.drawString(0 + x, 32 + y, "C");
    display.drawString(0 + x, 48 + y, "P");
    display.setFont(ArialMT_Plain_10);
    display.drawString(12 + x,  0 + y, "ynamic");
    display.drawString(12 + x, 14 + y, "ost");
    display.drawString(12 + x, 30 + y, "onfiguration");
    display.drawString(12 + x, 46 + y, "rotocol");
    display.drawString(12 + x, 54 + y, " ~ Simon M");

    display.drawXbm(116 + x, 32 + y, 16, 32, mace);

    display.display();
}

char* buf_lldp_str(lldp_str val, uint8_t len) {
    if (!len) len = val.len;
    for (uint16_t i = 0; i < len; ++i) {
        tlvbuffer[i] = val.str[i];
    } tlvbuffer[len] = '\0';
    return tlvbuffer;
}
char* buf_lldp_str(lldp_str val) {
    return buf_lldp_str(val, 0);
}

int cursor_x = 0;
int cursor_y = 0;
#define display_width 128
#define display_height 64

size_t ssdnewline() {
    Serial.println("");
    if (cursor_y >= display_height - 8) {
        cursor_x = 12;
        cursor_y = 0;
    }
    else {
        cursor_x  = 12;
        cursor_y += 8;
    }
    return 1;
}

size_t ssdwrite(uint8_t ch) {
    char printch[2] = {0};
    if (cursor_x > (cursor_y < 32 ? 128 : 112) - 6) {
        ssdnewline();
        if ((char)ch != '\n') {
            printch[0] = ch;
            display.drawString(cursor_x,  cursor_y, printch);
            cursor_x += display.getStringWidth(printch, 1);
        }
    }
    else {
        if (cursor_x < 12) {
            cursor_x = 12;
        }
        if ((char)ch != '\n') {
            printch[0] = ch;
            display.drawString(cursor_x,  cursor_y, printch);
            cursor_x += display.getStringWidth(printch, 1);
        }
        else {
            ssdnewline();
        }
    }
    return 1;
}

size_t ssdprint(const char ch[]) {
    Serial.print(ch);
    size_t n = 0;
    while (*ch)
    {
        n += ssdwrite(*ch);
        ch++;
    }
    return n;
}
          
size_t ssdprintln(const char *ch) {
    return ssdprint(ch) + ssdnewline();
}

void printLLDP() {
    // clear center, leave LLDP and bitmap on sides
    display.setColor(BLACK);
    display.fillRect(0,0,128,32);
    display.fillRect(12,0,100,64);
    display.setColor(WHITE);
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 0, "L");
    display.drawString(0, 16, "L");
    display.setFont(ArialMT_Plain_10);
    cursor_x = 12;
    cursor_y = 0;

    ssdprint("Port `");
    if (atoi(buf_lldp_str(packet.port.id))) {
        ssdwrite(port_subtype(packet.port.subtype));
        ssdwrite(' ');
        ssdprintln(tlvbuffer);
    }
    if (packet.port.description.len > 0) {
        ssdprintln(buf_lldp_str(packet.port.description));
    }
    if (packet.name.len > 0) {
        ssdprint("Name `");
        ssdprintln(capstring(packet.capabilities, tlvbuffer));
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.vlan.id) {
        ssdprint("VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
        ssdprintln(buf_lldp_str(packet.vlan.name));
    }
    if (packet.vlan2.id) {
        ssdprint("VLA2 ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
    }
    display.display();
}

void printCDP() {
    // clear center, leave CDP and bitmap on sides
    display.setColor(BLACK);
    display.fillRect(0,0,128,32);
    display.fillRect(12,0,100,64);
    display.setColor(WHITE);
    display.setFont(ArialMT_Plain_16);
    display.drawString(0, 16, "C");
    display.setFont(ArialMT_Plain_10);
    cursor_x = 12;
    cursor_y = 0;

    ssdprint("Port: ");
    if (buf_lldp_str(packet.port.id)) {
        ssdprintln(tlvbuffer);
    }
    if (packet.name.len > 0) {
        ssdprint("Name: ");
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.description.len > 0) {
        ssdprint("Model: ");
        ssdprintln(buf_lldp_str(packet.description));
    }
    if (packet.vlan.id) {
        ssdprint("Native VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprintln(tlvbuffer);
    }
    if (packet.vlan2.id) {
        ssdprint("Voice VLAN ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
    }
    display.display();
}

void printIP(byte ipoct[4], byte newline) {
    char octet[5];
    for (byte i=0;i<4;++i) {
        itoa(ipoct[i], octet, 10);
        ssdprint(octet);
        if (i < 3) ssdwrite('.');
    }
    if (newline) ssdwrite('\n');
}

void displayMAC(byte octets[6], byte ipoct[4], byte ippad) {
    // clear center, leave MACE and bitmap on sides
    display.setColor(BLACK);
    display.fillRect(12, 0, 102, ippad > 0 ? 64 : 44);
    display.fillRect(114, 0, 14, 32);
    display.setColor(WHITE);
    display.setFont(ArialMT_Plain_16);

    char octet[5];
    // MAC
    byte cury = 0;
    byte curx = 18;
    for (byte i=0;i<6;++i) {
        if (i == 3) {
          cury = 20;
          curx = 18;
        }

        cursor_x = curx + 2*14;
        cursor_y = cury;
        if ((i+1) % 3) ssdwrite(':');

        if (octets[i] >> 4) {
          itoa(octets[i], octet, 16);
        } else {
          octet[0] = '0';
          itoa(octets[i], octet + 1, 16);
        }
        //uppercase the chars the lazy way. saves fontspace
        char *s = octet;
        while (*s) {
          *s = toupper((unsigned char) *s);
          s++;
        }
        cursor_x = curx;
        cursor_y = cury;
        ssdprint(octet);
        curx = curx + 2 * 12;

        if ((i+1) % 3) curx = curx + 1 * 12 - 2;
    }
    // IP (small font)
    display.setFont(ArialMT_Plain_10);
    if (ippad > 0) {
        display.drawString(18, 44, "IP Address");
        cursor_x = 18;
        cursor_y = 54;
        printIP(ipoct, false);
    }
    display.display();
}

void printDHCP() {
    // clear center, leave DHCP and bitmap on sides
    display.setColor(BLACK);
    display.fillRect(12, 0, 100, 64);
    display.setColor(WHITE);
    display.setFont(ArialMT_Plain_10);
    cursor_x = 12;
    cursor_y = 0;
    
    ssdprint("ip:\n ");
    printIP(ether.myip, false);
    ssdprint("/");
    itoa(toCidr(ether.netmask), tlvbuffer, 10);
    ssdprint(tlvbuffer);
    ssdprint("\ngw:\n ");
    printIP(ether.gwip, true);
    ssdprint("dns:\n ");
    printIP(ether.dnsip, true);

    display.display();

    cursor_x = 66;
    cursor_y = 0;
    if (!ether.dnsLookup(dns_domain, true)) {
        Serial.println("\n[*] DNS Error");
        ssdprint("!DNS ERR!");
    }
    else {
        Serial.println("\n[*] DNS Received");
        display.setColor(BLACK);
        display.fillRect(cursor_x, 0, 8*9, 10);
        display.setColor(WHITE);
        cursor_x = 12;
        cursor_y = 46;
        ssdprint(dns_domain);
        ssdprint(":\n ");
        printIP(ether.hisip, false);
    }
    display.display();
}

volatile unsigned long menu_start = 0;
volatile unsigned long button_pressed = 0;
unsigned long completed = 0;
unsigned long packet_count = 0;
volatile byte menu_down = 0;

void ICACHE_RAM_ATTR menu_ISR() {
    unsigned long millis_value = millis();
    if (tool_selected == tool::MENU
        && button_pressed + 100 < millis_value
        && millis_value > 100
        ) {
        Serial.println("\n[!] Button pressed");
        // TODO: Better debounce for final button
        menu_down = 1;
        button_pressed = millis_value;
    }
    return;
}

void loop() {
    uint16_t len = 0;
    uint16_t result = 0;
    int x = 0; int y = 0;
    char countdown[2] = {0};

    if (tool_selected == tool::MENU || menu_start > 0) {
        if (tool_selected != tool::MENU || menu_start < 1) {
            menu_start = millis();
            tool_selected = tool::MENU;
            setup_menu();
        }
        if (menu_down) {
            menu_selected = (tool)((menu_selected + 1) % (TOOL_COUNT));
            display_menu();
            menu_down = 0;
        }
        for (byte i = MENU_SECONDS - 1; i > 0; --i) {
            if (menu_start + (MENU_SECONDS * 1000) - completed < (i * 1000) && menu_countdown > (i)) {
                menu_countdown = i;
                display.setFont(ArialMT_Plain_10);
                display.setColor(BLACK);
                display.fillRect(120 + x, 6 + y, 8, 10);
                display.setColor(WHITE);
                countdown[0] = menu_countdown + 0x30;
                display.drawString(120 + x, 6 + y, countdown);
                Serial.print("\r[>] Menu - ");
                Serial.print(countdown);
                display.display();
                break;
            }
        }
        if (menu_start + (MENU_SECONDS * 1000) < completed || (menu_start < 1 && tool_selected == tool::MENU)) {
            tool_selected = (tool)menu_selected;
            Serial.print("[>] Selected Mode - ");
            Serial.println(menuItems[menu_selected]);

            switch (tool_selected) {
                case tool::LLDP_CDP:
                    setup_lldp();
                    break;
                case tool::MACE:
                    setup_mac();
                    break;
                case tool::DHCP:
                    setup_dhcp();
                    break;
                default:
                    break;
            }
            menu_start = 0;
            menu_countdown = MENU_SECONDS;
        }
        completed = millis();
    }
    else {
        if (digitalRead(MENU_PIN) == HIGH) {
            menu_start = millis();
            completed = millis();
            Serial.println("");
            menu_ISR();
            return;
        }
        if (tool_selected == tool::MACE
            && completed + 1000 < millis()
            ) {
            ether.clientIcmpRequest(broadcast);
            completed = millis();
        }

        len = ether.packetReceive();
        if (!len) return;
        packet_count++;

        Serial.print("\r .  Packet ");
        Serial.print(packet_count);

        if (maceoffset < 1 && !macewrap) {
            ++maceoffset;
        }
        else if (maceoffset > -2 && macewrap) {
            --maceoffset;
        }
        else {
            macewrap = !macewrap;
        }
        display.setColor(BLACK);
        display.fillRect(116 + x - 2, 32 + y, 18, 32);
        display.setColor(WHITE);
        display.drawXbm(116 + x + maceoffset, 32 + y, 16, 32, mace);
        display.display();

        result = ether.packetLoop(len);
    }

    if (tool_selected == tool::LLDP_CDP) {
        if (result == 1 && readUShort(12) == ETHER_TYPE) {
            Serial.println("\n[*] LLDP Received");
            // LLDP
            parseLLDP(len, &packet);
            printLLDP();
            memset(&packet, 0, sizeof(lldp));
        }
        else if (result == 1 && checkCDP(len)) {
            Serial.println("\n[*] CDP Received");
            // CDP
            parseCDP(len, &packet);
            printCDP();
            memset(&packet, 0, sizeof(lldp));
        }
        else {
            // not LLDP or CDP
        }
        completed = millis();
    }
    if (tool_selected == tool::MACE) {
        if (len < 1) {
            completed = millis();
            return;
        }
        byte ippad = 0;
        if (ether.buffer[ETH_TYPE_H_P] == ETHTYPE_ARP_H_V &&
            ether.buffer[ETH_TYPE_L_P] == ETHTYPE_ARP_L_V) {
          ippad = ETH_ARP_SRC_IP_P;
        }
        else if (result == 0) {
            ippad = IP_SRC_P;
        }
        completed = millis();

        Serial.println("\n[*] MAC Address Received");
        displayMAC((byte *) ether.buffer + ETH_SRC_MAC, ether.buffer + ippad, ippad);
        Serial.println("");
    }

    if (tool_selected == tool::DHCP) {
        if (completed + 10000 > millis() && dhcp_result != 99) {
            return;
        }
        else {
            dhcp_result = ether.dhcpSetup();
        }
        if (dhcp_result < 1) {
            Serial.println("\n[!] Error with DHCP");
            display.drawString(60 + x, 0 + y, "!DHCP ERR!");
            display.display();
        }
        else {
            Serial.println("\n[*] DHCP Address Received");
            printDHCP();
            completed = millis();
        }
    }
}

