/* 
    MAC Address Reader - Microview + pings Version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <MicroView.h>
#include <EtherCard.h>

byte Ethernet::buffer[800];
static byte mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
static byte myip[] = { 192,168,0,42 };
static byte broadcast[] = { 0xff,0xff,0xff,0xff,0xff,0xff };
byte ethpin = 6;


void drawHeader();
void displayMAC(byte octets[6]);
void drawError(char* error);


void setup() {
  //Serial.begin(57600);
  //Serial.println("G'DAY");
  uView.begin();
  uView.clear(ALL);
  drawHeader();
  drawError("WAITING");
    uView.display();

  if (ether.begin(sizeof Ethernet::buffer, mymac, ethpin) == 0) {
    drawError("NO ETH ");
    uView.display();
    delay(5000);
    while(1) {
      displayMAC(mymac);
      delay(100);
    }
  }
  
  ether.staticSetup(myip, broadcast);
  ether.enablePromiscuous();
  ether.enableBroadcast();
}

static byte jelly = 1;
void drawHeader() {
  uView.clear(PAGE);
  // header text
  uView.setFontType(1);
  uView.setCursor(0, 0);
  uView.print("MAC");
  // jellyfish
  int fw1 = uView.getFontWidth();
  uView.setFontType(5);
  uView.drawChar(fw1 * 4 + 4, 0, 48 + (++jelly % 2));
  // reset
  uView.setFontType(1);
  uView.setCursor(0, 0);
}

void drawError(char* error) {
  uView.setFontType(1);
  uView.setCursor(0, 20);
  uView.print(error);
}

void displayMAC(byte octets[6]) {
  drawHeader();
  if (!octets) {
    drawError("NULL   MAC");
    uView.display();
    return;
  }
  
  char octet[3];
  int fw = uView.getFontWidth();
  int fh = uView.getFontHeight();
  for (int i=0;i<6;++i) {
    uView.setFontType(1);
    // sort out positioning of octet
    int curW = fw * ((i % 3) * 2.5);
    int curH = 20 + (i > 2 ? fh : 0);
    uView.setCursor(curW, curH);
    sprintf(octet, "%02x", octets[i]);
    uView.print(octet);
    
    uView.setFontType(0);
    int fc = (fh / 2) - (uView.getFontHeight() / 2);
    uView.setCursor(curW + fw * 2 - 1, curH + fc);
    // only display colon when elegant
    if ((i + 1) % 3) uView.print(":");
  }

  uView.display();
}

static byte done = 0;
void loop() {
  if (done || ether.packetReceive()) {
    done = 1;
    displayMAC((byte *) ether.buffer + ETH_SRC_MAC);
    delay(100);
  } else {
    ether.clientIcmpRequest(broadcast);
    delay(300);
  }
}
