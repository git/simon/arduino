/* 
    LLDP Reader - Microview Version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/


#include <MicroView.h>
#include <EtherCard.h>
#include "lldp_packet.h"

byte Ethernet::buffer[800];
const char lldpdhcp[] PROGMEM = "\x01\x01\x06\x00\x58\x51\xf4\x2d\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x28\xfd\x80\xb0\x22\xb3\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x63\x82\x53\x63\x35\x01\x01\x39\x02\x05\xdc\x37\x04\x01\x03\x1c\x06\xff\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00";

static byte mymac[] = { 0x28,0xfd,0x80,0xb0,0x22,0xb3 };
static byte broadip[] = { 255,255,255,255 };
static byte myip[] = { 0,0,0,0 };
byte ethpin = 6;

void drawHeader();
void drawError(char* error);


void setup() {
  uView.begin();
  uView.clear(ALL);
  drawHeader();
  uView.display();

  if (ether.begin(sizeof Ethernet::buffer, mymac, ethpin) == 0) {
    drawError("NO ETH");
    uView.display();
    while(1);
  }
  
  ether.staticSetup(myip);
  ether.enablePromiscuous();
  ether.enableBroadcast();
  ether.enableMulticast();
  
  ether.sendUdp(lldpdhcp, 317L, 68, broadip, 67);
}

static byte jelly = 1;
void drawHeader() {
  uView.clear(PAGE);
  // header text
  uView.setFontType(1);
  uView.setCursor(0, 0);
  uView.println("LLDP\n~SimonMWaiting");
  // jellyfish
  int fw1 = uView.getFontWidth();
  uView.setFontType(5);
  uView.drawChar(fw1 * 4 + 4, 0, 48 + (++jelly % 2));
  // reset
  uView.setFontType(1);
  uView.setCursor(0, 0);
}

void drawError(char* error) {
  uView.setFontType(1);
  uView.setCursor(0, 0);
  uView.println(error);
}

uint16_t readUShort(int position, int fromBit, int count) {
    uint8_t *dw = const_cast<uint8_t *>((byte*)ether.buffer + position);
    fromBit %= 8;
    count %= 16;        // output unsigned short has only 16 bits

    return (uint16_t)
    // Bitwise OR of shifted first byte left and second byte shifted rigth + folding left
    ((((((uint8_t)(dw[0] << fromBit) | (uint8_t)(dw[1] >> (8 - fromBit)))    << 8) |
    // Bitwise OR of shifted second byte left and third byte shifted rigth
    ((uint8_t)(dw[1] << fromBit) | (uint8_t)(dw[2] >> (8 - fromBit)))))
    // Skipping bits on the right
    >> (16 - count));
}

uint16_t readUShort(int position) {
    return *(uint16_t *)((byte*)ether.buffer + position);
}

uint8_t readUChar(int position, int fromBit, int count) {
    uint8_t *db = const_cast<uint8_t *>((byte*)ether.buffer + position);
    fromBit %= 8;
    count %= 8;

    return (uint8_t)(((uint8_t)(*db << fromBit) | (uint8_t)(db[1] >> (8 - fromBit))) >> (8 - count));
}
                                     
const char port_subtype(byte subtype) {
    switch (subtype) {
        case pID_types::interfaceAlias:
            return 'A';
        case pID_types::portComponent:
            return 'P';
        case pID_types::macAddress:
            return 'M';
        case pID_types::networkAddress:
            return 'N';
        case pID_types::interfaceName:
            return 'I';
        case pID_types::agentCircuitID:
            return 'C';
        case pID_types::locallyAssigned:
            return 'L';
    }
    return '?';
}

char tlvbuffer[MAX_VALUE_SIZE];
char* buf_lldp_str(lldp_str val, byte len) {
    if (!len) len = val.len;
    for (uint16_t i = 0; i < len; ++i) {
        tlvbuffer[i] = val.str[i];
    } tlvbuffer[len] = '\0';
    return tlvbuffer;
}
char* buf_lldp_str(lldp_str val) {
    return buf_lldp_str(val, 0);
}
char* capstring(lldp_caps c) {
    int8_t i = 0;
    if (c.caps & capabilities::other)
        tlvbuffer[i++] = (c.enabled & capabilities::other)     ? 'O' : 'o';
    if (c.caps & capabilities::repeater)
        tlvbuffer[i++] = (c.enabled & capabilities::repeater)  ? 'P' : 'p';
    if (c.caps & capabilities::bridge)
        tlvbuffer[i++] = (c.enabled & capabilities::bridge)    ? 'B' : 'b';
    if (c.caps & capabilities::wlanAP)
        tlvbuffer[i++] = (c.enabled & capabilities::wlanAP)    ? 'W' : 'w';
    if (c.caps & capabilities::router)
        tlvbuffer[i++] = (c.enabled & capabilities::router)    ? 'R' : 'r';
    if (c.caps & capabilities::telephone)
        tlvbuffer[i++] = (c.enabled & capabilities::telephone) ? 'T' : 't';
    if (c.caps & capabilities::DOCSIS)
        tlvbuffer[i++] = (c.enabled & capabilities::DOCSIS)    ? 'D' : 'd';
    if (c.caps & capabilities::station)
        tlvbuffer[i++] = (c.enabled & capabilities::station)   ? 'S' : 's';
    tlvbuffer[i] = 0;
    return tlvbuffer;
}

static lldp packet;
void loop() {    
    uint16_t len = ether.packetReceive();
    if (!len) return;

    uint16_t pos = ether.packetLoop(len);
    if (pos != 1 || readUShort(12) != ETHER_TYPE) return;

    uint16_t length = 0;
    uint16_t position = 14;
    
    while (position + 1 < len) {
        TLV_types type = (TLV_types)readUChar(position, 0, 7);
        length = readUShort(position, 7, 9);

        // test whether length of TLV exceeds the end of packet
        if (position + TL_SIZE + length >= len) {
            break;
        }
        if (type == endOfLLPDU) {
            break;
        }

        if (length < 1) {
            position += TL_SIZE;
            continue;
        }

        byte* tlv_data = ether.buffer + position + TL_SIZE;
        switch (type) {
            case TLV_types::chassisID:
                break;
            case TLV_types::portID:
                packet.port.subtype = *tlv_data;
                packet.port.id.str = (char*)tlv_data + 1;
                packet.port.id.len = length - 1;
                break;
            case TLV_types::timeToLive:
                break;
            case TLV_types::portDescription:
                packet.port.description.str = (char*)tlv_data;
                packet.port.description.len = length;
                break;
            case TLV_types::systemName:
                packet.name.str = (char*)tlv_data;
                packet.name.len = length;
                break;
            case TLV_types::systemDescription:
                packet.description.str = (char*)tlv_data;
                packet.description.len = length;
                break;
            case TLV_types::systemCapabilities:
                packet.capabilities.caps = *(tlv_data + 1);
                packet.capabilities.enabled = *(tlv_data + 1 + 2);
                break;
            case TLV_types::managementAddress:
                break;
            case TLV_types::orgSpecific: {
                byte matchieee = true;
                byte matchtele = true;
                for (byte i = 0; i < ORG_SPEC_LEN; ++i) {
                    if (tlv_data[i] != ieee[i]) matchieee = false;
                    if (tlv_data[i] != tele[i]) matchtele = false;
                }
                byte subtype = *(tlv_data + ORG_SPEC_LEN);
                lldp_vlan* vlan = &packet.vlan;
                if (matchtele) {
                    if (subtype != 0x02) break;
                    byte apptype = *(tlv_data + ORG_SPEC_LEN + 1);
                    uint16_t vlanpos = position + TL_SIZE + ORG_SPEC_LEN + 2;
                    byte tag = readUChar(vlanpos, 1, 1); // 1 or 0

                    uint16_t vlanid = readUShort(vlanpos, 3, 12);
                    if (packet.vlan.id != vlanid && packet.vlan.id > 0)
                        vlan = &packet.vlan2;
                    vlan->id = vlanid;
                }
                if (matchieee) {
                    if (subtype == 0x01 || subtype == 0x03) {
                        byte* vlanptr = tlv_data + ORG_SPEC_LEN + 1;
                        uint16_t vlanid = (((uint16_t)*vlanptr)<<8) | (uint16_t)*(vlanptr + 1);
                        if (packet.vlan.id != vlanid && packet.vlan.id > 0)
                            vlan = &packet.vlan2;
                        vlan->id = vlanid;
                        if (subtype == 0x03) {
                            vlan->name.len = *(vlanptr + 2);
                            vlan->name.str = (char*)vlanptr + 3;
                        }
                    }
                }
                break;
            }
            default:
            break;
        }
        // move position to the start of next TLV
        position += length + TL_SIZE;
    }
    uView.clear(PAGE);
    uView.setFontType(0);
    uView.setCursor(0, 0);
    uView.print("P");
    if (atoi(buf_lldp_str(packet.port.id))) {
		uView.print(port_subtype(packet.port.subtype));
        uView.print(tlvbuffer);
        uView.print(";");
    }
    uView.println(buf_lldp_str(packet.port.description));
    uView.print("Name `");
    uView.println(capstring(packet.capabilities));
    uView.println(buf_lldp_str(packet.name));
    if (packet.vlan.id) {
        uView.print("VLAN ");
        uView.println(packet.vlan.id);
        //uView.println(buf_lldp_str(packet.vlan.name));
    }
    if (packet.vlan2.id) {
        uView.print("VLAN2 ");
        uView.println(packet.vlan2.id);
        //uView.println(buf_lldp_str(packet.vlan2.name));
    } else {
        uView.println(buf_lldp_str(packet.vlan.name));
    }
    uView.display();

    memset(&packet, 0, sizeof(lldp));
}
