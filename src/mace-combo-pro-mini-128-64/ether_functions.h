/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ETHER_FUNCTIONS_H
#define ETHER_FUNCTIONS_H

    uint16_t readUShort(int position, int fromBit, int count);
    uint16_t readUShort(int position);
    uint8_t readUChar(int position, int fromBit, int count);
    uint8_t readUChar(int position);

    uint8_t toCidr(uint8_t ipbytes[4]);

#endif
