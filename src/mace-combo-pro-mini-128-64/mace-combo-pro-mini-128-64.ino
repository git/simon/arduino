/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* 
    Using Startup Menu:
    Connect pin 2 to ground using a button. This will rotate through the 
    available tools.

    Diagnostics:
    No Mace in lower right corner = no Ethernet module communications.
    Mace moving side to side = packets
    Mace not moving = no packets
*/

#include <SPI.h>
#include <Wire.h>
#include <EtherCard.h>
#include <lcdgfx.h>
#include <lcdgfx_gui.h>
#include "scp_12x16.h"
#include "lldp_parse.h"
#include "cdp_parse.h"
#include "ether_functions.h"

/*
 * Ethernet parameters
 */
uint8_t Ethernet::buffer[800];
const uint8_t mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
const uint8_t myip[] = { 192,168,102,142 };
const char dns_domain[] = "molzy.com";
static byte broadcast[] = { 0xff,0xff,0xff,0xff,0xff,0xff };
#define ETH_PIN 9

char tlvbuffer[MAX_VALUE_SIZE];
static lldp packet;
uint8_t dhcp_result = 99;

/*
 * Tool selection menu
 */

// Interrupt-capable pin for menu navigation button
#define MENU_PIN 2

// Menu countdown (0-9)
#define MENU_SECONDS 5

enum tool {
    MENU      = 99,
    LLDP_CDP  = 0,
    MACE      = 1,
    DHCP      = 2,
};
tool tool_selected = tool::MENU;
static byte menu_countdown = MENU_SECONDS;

const char *menuItems[] =
{
    "LLDP / CDP", // LLDP / CDP
    "MAC Address", // MACE
    "DHCP Client", // DHCP
};

// Menu state
LcdGfxMenu menu( menuItems, sizeof(menuItems) / sizeof(char *) , { {4, 0}, {112, 64} } );

/*
 * Mace bitmap
 */
const uint8_t mace[] PROGMEM = {
  0x00, 0x30, 0xF8, 0xE8, 0xC8, 0x1E, 0xFB, 0x1E, 0xC8, 0xE8, 0xF8, 0x30, 0x00, 
  0x00, 0x00, 0x00, 0x13, 0x3F, 0xEE, 0x6D, 0xAE, 0x3F, 0x13, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x00, 0xDD, 0x66, 0xBB, 0x00, 0x00, 0x00, 0x00, 0x00, 
  0x00, 0x00, 0x00, 0x00, 0x30, 0x4D, 0xA6, 0x4B, 0x30, 0x00, 0x00, 0x00, 0x00, 
};

int8_t maceoffset = 0;
uint8_t macewrap = false;

/*
 * SSD1306 Display functions
 */
class DisplaySSD1306_128x64_SPI_CursorGetters: public  DisplaySSD1306_128x64_SPI
{
  public:
    explicit DisplaySSD1306_128x64_SPI_CursorGetters(int8_t rstPin, const SPlatformSpiConfig &config = {-1, {-1}, -1, 0, -1, -1})
        : DisplaySSD1306_128x64_SPI(rstPin, config)
    {
    }
    uint8_t getTextCursorX()
    {
      return (uint8_t)m_cursorX;
    }
    uint8_t getTextCursorY()
    {
      return (uint8_t)m_cursorY;
    }
};

DisplaySSD1306_128x64_SPI_CursorGetters display(6,{-1, 8, 7, 0,-1,-1});

// Text drawing functions
size_t ssdnewline();
size_t ssdwrite(uint8_t ch);
size_t ssdprint(const char ch[]);
size_t ssdprintln(const char *ch);

/*
 * Setup and ISR functions
 */
void menu_ISR();
void setup_menu();
void setup_lldp();
void setup_mac();
void setup_dhcp();


void setup() {
    pinMode(MENU_PIN, INPUT_PULLUP);

    display.setFixedFont(ssd1306xled_font6x8);
    display.begin();

    /* remove below two lines to flip display */
    display.getInterface().flipVertical(1);
    display.getInterface().flipHorizontal(1);

    display.clear();

    if (ether.begin(sizeof Ethernet::buffer, mymac, ETH_PIN) == 0) {
        display.setTextCursor(48, 0);
        ssdprint("!ERR!");
        display.setTextCursor(36, 8);
        ssdprint("!NO  ETH!");
        for(;;) {
            delay(100);
        }
    }

    tool_selected = tool::MENU;
    setup_menu();

    attachInterrupt(digitalPinToInterrupt(MENU_PIN), menu_ISR, FALLING);
}

void setup_menu() {
    display.clear();
    display.setFixedFont(ssd1306xled_font6x8);
    menu.show(display);
    display.drawBitmap1(114, 32, 13, 32, mace);
    display.setTextCursor(120, 6);
    display.printChar((uint8_t)(menu_countdown + 0x30));
}

void setup_lldp() {
    ether.staticSetup(myip);
    ether.enablePromiscuous();
    ether.enableBroadcast();
    ether.enableMulticast();

    display.clear();

    display.setFixedFont(Source_Code_Pro12x16);
    display.setTextCursor(0, 0);
    display.printChar('>'); // L
    display.setTextCursor(0, 16);
    display.printChar('>'); // L
    display.setTextCursor(0, 32);
    display.printChar('D'); // D
    display.setTextCursor(0, 48);
    display.printChar('@'); // P
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);
    ssdprint("ink");
    display.setTextCursor(12, 16);
    ssdprint("ayer");
    display.setTextCursor(12, 32);
    ssdprint("iscovery");
    display.setTextCursor(12, 48);
    ssdprint("rotocol");
    display.setTextCursor(12, 56);
    ssdprint(" ~ Simon M");

    display.drawBitmap1(114, 32, 13, 32, mace);
}

void setup_mac() {
    ether.staticSetup(myip);
    ether.enablePromiscuous();
    ether.enableBroadcast();
    ether.enableMulticast();

    display.clear();

    display.setFixedFont(Source_Code_Pro12x16);
    display.setTextCursor(0, 0);
    display.printChar('='); // M
    display.setTextCursor(0, 16);
    display.printChar('A'); // A
    display.setTextCursor(0, 32);
    display.printChar('C'); // C
    display.setTextCursor(0, 48);
    display.printChar('E'); // E
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);
    ssdprint("ac");
    display.setTextCursor(12, 16);
    ssdprint("ddress");
    display.setTextCursor(12, 32);
    ssdprint("ompact");
    display.setTextCursor(12, 48);
    ssdprint("xtractor");
    display.setTextCursor(12, 56);
    ssdprint(" ~ Simon M");

    display.drawBitmap1(114, 32, 13, 32, mace);
}

void setup_dhcp() {
    display.clear();

    display.setFixedFont(Source_Code_Pro12x16);
    display.setTextCursor(0, 0);
    display.printChar('D'); // D
    display.setTextCursor(0, 16);
    display.printChar('?'); // H
    display.setTextCursor(0, 32);
    display.printChar('C'); // C
    display.setTextCursor(0, 48);
    display.printChar('@'); // P
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);
    ssdprint("ynamic");
    display.setTextCursor(12, 16);
    ssdprint("ost");
    display.setTextCursor(12, 32);
    ssdprint("onfiguration");
    display.setTextCursor(12, 48);
    ssdprint("rotocol");
    display.setTextCursor(12, 56);
    ssdprint(" ~ Simon M");

    display.drawBitmap1(114, 32, 13, 32, mace);
    
    ether.enableBroadcast();
}

char* buf_lldp_str(lldp_str val, uint8_t len) {
    if (!len) len = val.len;
    for (uint16_t i = 0; i < len; ++i) {
        tlvbuffer[i] = val.str[i];
    } tlvbuffer[len] = '\0';
    return tlvbuffer;
}
char* buf_lldp_str(lldp_str val) {
    return buf_lldp_str(val, 0);
}

size_t ssdnewline() {
    if (display.getTextCursorY() >= display.height() - 8) {
        display.setTextCursor(12, 0);
    }
    else {
        display.setTextCursor(12, display.getTextCursorY() + 8);
    }
    return 1;
}

size_t ssdwrite(uint8_t ch) {
    if (display.getTextCursorX() > (display.getTextCursorY() < 32 ? 128 : 112) - 6) {
        ssdnewline();
        if ((char)ch != '\n') {
            display.printChar((char)ch);
        }
    }
    else {
        if (display.getTextCursorX() < 12) {
            display.setTextCursor(12, display.getTextCursorY());
        }
        if ((char)ch != '\n') {
            display.printChar((char)ch);
        }
        else {
            ssdnewline();
        }
    }
    return 1;
}

size_t ssdprint(const char ch[]) {
    size_t n = 0;
    while (*ch)
    {
        n += ssdwrite(*ch);
        ch++;
    }
    return n;
}
          
size_t ssdprintln(const char *ch) {
    return ssdprint(ch) + ssdnewline();
}

void printLLDP() {
    // clear center, leave LLDP and bitmap on sides
    display.setColor(0x0000);
    display.fillRect(0,0,128,24);
    display.fillRect(12,0,112,64);
    display.setColor(0xFFFF);
    display.setFixedFont(Source_Code_Pro12x16);
    display.setTextCursor(0, 0);
    display.printChar('>'); // L
    display.setTextCursor(0, 16);
    display.printChar('>'); // L
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);

    ssdprint("Port `");
    if (atoi(buf_lldp_str(packet.port.id))) {
        ssdwrite(port_subtype(packet.port.subtype));
        ssdwrite(' ');
        ssdprintln(tlvbuffer);
    }
    if (packet.port.description.len > 0) {
        ssdprintln(buf_lldp_str(packet.port.description));
    }
    if (packet.name.len > 0) {
        ssdprint("Name `");
        ssdprintln(capstring(packet.capabilities, tlvbuffer));
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.vlan.id) {
        ssdprint("VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
        ssdprintln(buf_lldp_str(packet.vlan.name));
    }
    if (packet.vlan2.id) {
        ssdprint("VLA2 ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
        ssdwrite(' ');
    }
}

void printCDP() {
    // clear center, leave CDP and bitmap on sides
    display.setColor(0x0000);
    display.fillRect(0,0,128,24);
    display.fillRect(12,0,112,64);
    display.setColor(0xFFFF);
    display.setFixedFont(Source_Code_Pro12x16);
    display.setTextCursor(0, 16);
    display.printChar('C'); // C
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);

    ssdprint("Port: ");
    if (buf_lldp_str(packet.port.id)) {
        ssdprintln(tlvbuffer);
    }
    if (packet.name.len > 0) {
        ssdprint("Name: ");
        ssdprintln(buf_lldp_str(packet.name));
    }
    if (packet.description.len > 0) {
        ssdprint("Model: ");
        ssdprintln(buf_lldp_str(packet.description));
    }
    if (packet.vlan.id) {
        ssdprint("Native VLAN ");
        itoa(packet.vlan.id, tlvbuffer, 10);
        ssdprintln(tlvbuffer);
    }
    if (packet.vlan2.id) {
        ssdprint("Voice VLAN ");
        itoa(packet.vlan2.id, tlvbuffer, 10);
        ssdprint(tlvbuffer);
    }
}

void printIP(byte ipoct[4], byte newline) {
    char octet[5];
    for (byte i=0;i<4;++i) {
        itoa(ipoct[i], octet, 10);
        ssdprint(octet);
        if (i < 3) ssdwrite('.');
    }
    if (newline) ssdwrite('\n');
}

void displayMAC(byte octets[6], byte ipoct[4], byte ippad) {
    // clear center, leave MACE and bitmap on sides
    display.setColor(0x0000);
    display.fillRect(12,0,112, ippad > 0 ? 64 : 40);
    display.setColor(0xFFFF);
    display.setFixedFont(Source_Code_Pro12x16);

    char octet[5];
    // MAC
    byte cury = 0;
    byte curx = 18;
    for (byte i=0;i<6;++i) {
        if (i == 3) {
          cury = 24;
          curx = 18;
        }

        display.setTextCursor(curx + 2*12, cury);
        if ((i+1) % 3) ssdwrite(':');

        if (octets[i] >> 4) {
          itoa(octets[i], octet, 16);
        } else {
          octet[0] = '0';
          itoa(octets[i], octet + 1, 16);
        }
        //uppercase the chars the lazy way. saves fontspace
        char *s = octet;
        while (*s) {
          *s = toupper((unsigned char) *s);
          s++;
        }
        display.setTextCursor(curx, cury);
        ssdprint(octet);
        curx = curx + 2 * 12;

        if ((i+1) % 3) curx = curx + 1 * 12 - 2;
    }
    // IP (small font)
    display.setFixedFont(ssd1306xled_font6x8);
    if (ippad > 0) {
        display.setTextCursor(18, 48);
        ssdprint("IP Address");
        display.setTextCursor(18, 56);
        printIP(ipoct, false);
    }
}

void printDHCP() {
    // clear center, leave DHCP and bitmap on sides
    display.setColor(0x0000);
    display.fillRect(12,0,112,64);
    display.setColor(0xFFFF);
    display.setFixedFont(ssd1306xled_font6x8);
    display.setTextCursor(12, 0);

    ssdprint("ip:\n ");
    printIP(ether.myip, false);
    ssdwrite('/');
    itoa(toCidr(ether.netmask), tlvbuffer, 10);
    ssdprint(tlvbuffer);
    ssdprint("\ngw:\n ");
    printIP(ether.gwip, true);
    ssdprint("dns:\n ");
    printIP(ether.dnsip, true);

    for(;;) {
        display.setTextCursor(66, 0);
        if (!ether.dnsLookup(dns_domain, true)) {
            ssdprint("!DNS ERR!");
        }
        else {
            ssdprint("         ");
            display.setTextCursor(12, 48);
            ssdprint(dns_domain);
            ssdprint(":\n ");
            printIP(ether.hisip, false);
            break;
        }
    }
}

volatile unsigned long menu_start = 0;
volatile unsigned long button_pressed = 0;
unsigned long completed = 0;
volatile byte menu_down = 0;

void menu_ISR() {
    unsigned long millis_value = millis();
    if (tool_selected == tool::MENU
        && button_pressed + 50 < millis_value
        && millis_value > 100
        ) {
        // TODO: Better debounce for final button
        menu_down = 1;
        button_pressed = millis_value;
    }
    return;
}

void loop() {
    uint16_t len = 0;
    uint16_t result = 0;

    if (tool_selected == tool::MENU || menu_start > 0) {
        if (menu_down) {
            menu.down();
            menu.show(display);
            menu_down = 0;
        }
        if (tool_selected != tool::MENU || menu_start < 1) {
            menu_start = millis();
            tool_selected = tool::MENU;
            setup_menu();
        }
        for (byte i = MENU_SECONDS - 1; i > 0; --i) {
            if (menu_start + (MENU_SECONDS * 1000) - completed < (i * 1000) && menu_countdown > (i)) {
                menu_countdown = i;
                display.setFixedFont(ssd1306xled_font6x8);
                display.setTextCursor(120, 6);
                display.printChar((uint8_t)(menu_countdown + 0x30));
                break;
            }
        }
        if (menu_start + (MENU_SECONDS * 1000) < completed || (menu_start < 1 && tool_selected == tool::MENU)) {
            tool_selected = (tool)menu.selection();
            switch (tool_selected) {
                case tool::LLDP_CDP:
                    setup_lldp();
                    break;
                case tool::MACE:
                    setup_mac();
                    break;
                case tool::DHCP:
                    setup_dhcp();
                    break;
                default:
                    break;
            }
            menu_start = 0;
        }
        completed = millis();
    }
    else {
        if (tool_selected == tool::MACE
            && completed + 1000 < millis()
            ) {
            ether.clientIcmpRequest(broadcast);
            completed = millis();
        }
        len = ether.packetReceive();
        if (!len) return;

        if (completed + 30 < millis()) {
            if (maceoffset < 2 && !macewrap) {
                ++maceoffset;
            }
            else if (maceoffset > -2 && macewrap) {
                --maceoffset;
            }
            else {
                macewrap = !macewrap;
            }
            display.drawBitmap1(114 + maceoffset, 32, 13, 32, mace);
        }

        result = ether.packetLoop(len);
    }

    if (tool_selected == tool::LLDP_CDP) {
        if (result == 1 && readUShort(12) == ETHER_TYPE) {
            // LLDP
            parseLLDP(len, &packet);
            printLLDP();
            memset(&packet, 0, sizeof(lldp));
        }
        else if (result == 1 && checkCDP(len)) {
            // CDP
            parseCDP(len, &packet);
            printCDP();
            memset(&packet, 0, sizeof(lldp));
        }
        else {
            // not LLDP or CDP
        }
        completed = millis();
    }
    if (tool_selected == tool::MACE) {
        if (len < 1) {
            completed = millis();
            return;
        }
        byte ippad = 0;
        if (ether.buffer[ETH_TYPE_H_P] == ETHTYPE_ARP_H_V &&
            ether.buffer[ETH_TYPE_L_P] == ETHTYPE_ARP_L_V) {
          ippad = ETH_ARP_SRC_IP_P;
        }
        else if (result == 0) {
            ippad = IP_SRC_P;
        }
        completed = millis();

        displayMAC((byte *) ether.buffer + ETH_SRC_MAC, ether.buffer + ippad, ippad);
    }

    if (tool_selected == tool::DHCP) {
        if (completed + 10000 > millis() && dhcp_result != 99) {
            return;
        }
        dhcp_result = ether.dhcpSetup();
        if (dhcp_result < 1) {
            display.setTextCursor(60, 0);
            ssdprint("!DHCP ERR!");
        }
        else {
            printDHCP();
            completed = millis();
        }
    }
}

