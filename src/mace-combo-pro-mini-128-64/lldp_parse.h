/* 
    LLDP + CDP + MAC Address + DHCP Reader - Arduino Pro Mini + SSD1306 version
    Parts of this file are adapted from https://github.com/ITman1/lldp-cdp-sniffer
    These parts are Copyright (c) Radim Loskot xlosko01(at)stud.fit.vutbr.cz
    Original file:
    https://github.com/ITman1/lldp-cdp-sniffer/blob/master/src/lib/sniffers/packets/lldp_packet.h

    Additions and modifications are Copyright (c) 2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LLDP_PARSE_H
#define LLDP_PARSE_H

    #include <Arduino.h>

    static const uint16_t ETHER_TYPE = 0xCC88;       /**< Ethernet type for LLDP packet rev endianness */
    static const byte TL_SIZE = 2;                   /**< Size of type-value items */
    static const uint16_t MAX_VALUE_SIZE = 256;      /**< Max size of value item */
    static const byte ORG_SPEC_LEN = 3;              /**< Max size of value item */
    static const byte ieee[ORG_SPEC_LEN] = {0x00, 0x80, 0xc2};
    static const byte tele[ORG_SPEC_LEN] = {0x00, 0x12, 0xbb};

    typedef struct lldp_str {
        char* str;
        byte len;
    } lldp_str;
    typedef struct lldp_mac {
        byte m1; byte m2; byte m3;
        byte m4; byte m5; byte m6;
    } lldp_mac;
    typedef struct lldp_port {
        byte subtype;
        lldp_str id;
        lldp_str description;
    } lldp_port;
    typedef struct lldp_caps {
        uint8_t caps;
        uint8_t enabled;
    } lldp_caps;
    typedef struct lldp_vlan {
        uint16_t id;
        lldp_str name;
    } lldp_vlan;
    typedef struct lldp {
        lldp_str name;
        lldp_str description;
        lldp_port port;
        lldp_caps capabilities;
        lldp_vlan vlan;
        lldp_vlan vlan2;
    } lldp;

    /**
      * Enumeration of all TLV types of LLDP packet with corresponding value in packet
      */
    enum TLV_types {
        endOfLLPDU             = 0,
        chassisID              = 1,
        portID                 = 2,
        timeToLive             = 3,
        portDescription        = 4,
        systemName             = 5,
        systemDescription      = 6,
        systemCapabilities     = 7,
        managementAddress      = 8,
        orgSpecific            = 127
    };

    enum pID_types {
        interfaceAlias     = 1,
        portComponent      = 2,
        macAddress         = 3,
        networkAddress     = 4,
        interfaceName      = 5,
        agentCircuitID     = 6,
        locallyAssigned    = 7
    };

    enum cID_types {
        cchassisComponent   = 1,
        cinterfaceAlias     = 2,
        cportComponent      = 3,
        cmacAddress         = 4,
        cnetworkAddress     = 5,
        cinterfaceName      = 6,
        cagentCircuitID     = 7,
        clocallyAssigned    = 8
    };

    enum capabilities {
        other      = 0x01,
        repeater   = 0x02,
        bridge     = 0x04,
        wlanAP     = 0x08,
        router     = 0x10,
        telephone  = 0x20,
        DOCSIS     = 0x40,
        station    = 0x80,
    };

    void parseLLDP(uint16_t len, lldp *packet);
    char port_subtype(uint8_t subtype);
    char* capstring(lldp_caps c, char* tlvbuffer);
#endif
