/* 
    MAC Address Reader - Arduino Pro Mini / Pro Micro + ssd1306 Version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* for a 128x64 size display using SPI to communicate */

#include "ssd1306.h"
#include "nano_gfx.h"
#include <SPI.h>
#include <Wire.h>
#include <EtherCard.h>
#include <avr/pgmspace.h>
#include "scp_12x16.h"

byte Ethernet::buffer[800];
static byte mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
static byte myip[] = { 192,168,102,142 };
static byte broadcast[] = { 0xff,0xff,0xff,0xff,0xff,0xff };
#define ethpin 9

#define spaceguy1 &Source_Code_Pro12x16[556]
#define spaceguy2 &Source_Code_Pro12x16[604]
#define mace &Source_Code_Pro12x16[652]

void displayMAC(byte octets[6], byte ipoct[4]);

void setup() {
  ssd1306_128x64_spi_init(6, 8, 7);
  ssd1306_flipVertical(1);
  ssd1306_flipHorizontal(1);
  ssd1306_clearBlock(12,0,116,64);

  ssd1306_setFixedFont(Source_Code_Pro12x16);
  ssd1306_printFixed(0, 0, "M", STYLE_NORMAL);
  ssd1306_printFixed(0, 16, "A", STYLE_NORMAL);
  ssd1306_printFixed(0, 32, "C", STYLE_NORMAL);
  ssd1306_printFixed(0, 48, "E", STYLE_NORMAL);
  ssd1306_setFixedFont(ssd1306xled_font6x8);
  ssd1306_printFixed(12, 0, "AC", STYLE_NORMAL);
  ssd1306_printFixed(12, 16, "ddress", STYLE_NORMAL);
  ssd1306_printFixed(12, 32, "ompact", STYLE_NORMAL);
  ssd1306_printFixed(12, 48, "xtractor", STYLE_NORMAL);
  ssd1306_printFixed(16, 56, "Luke made this", STYLE_NORMAL);

  ssd1306_drawBitmap(104, 6, 24, 16, spaceguy2);
  ssd1306_drawBitmap(117, 0, 11, 32, mace);

  if (ether.begin(sizeof Ethernet::buffer, mymac, ethpin) == 0) {
    delay(5000);
    displayMAC(mymac, myip);
  }

  ether.staticSetup(myip, broadcast);
  ether.enablePromiscuous();
  ether.enableBroadcast();
}

void displayMAC(byte octets[6], byte ipoct[4]) {
  // clear center, leave MACE and bitmaps on sides
  ssd1306_clearBlock(12,0,92,64);

  char octet[5];
  // MAC
  byte cury = 0;
  byte curx = 18;
  ssd1306_setFixedFont(Source_Code_Pro12x16);
  for (byte i=0;i<6;++i) {
    if (i == 3) {
      cury = 24;
      curx = 18;
    }

    if ((i+1) % 3) ssd1306_printFixed(curx + 2*12, cury, ":", STYLE_NORMAL);

    if (octets[i] >> 4) {
      itoa(octets[i], octet, 16);
    } else {
      octet[0] = '0';
      itoa(octets[i], octet + 1, 16);
    }
	//uppercase the chars the lazy way. saves fontspace
	char *s = octet;
    while (*s) {
      *s = toupper((unsigned char) *s);
      s++;
    }
    ssd1306_printFixed(curx, cury, octet, STYLE_NORMAL);
	curx = curx + 2 * 12;

    if ((i+1) % 3) curx = curx + 1 * 12 - 2;
  }
  // IP (small font)
  cury = 56;
  curx = 14;
  ssd1306_setFixedFont(ssd1306xled_font6x8);
  ssd1306_printFixed(curx, 48, "IP Address", STYLE_NORMAL);
  for (byte i=0;i<4;++i) {
    itoa(ipoct[i], octet, 10);
    byte chars = ssd1306_printFixed(curx, cury, octet, STYLE_NORMAL);
	curx = curx + (chars + 1) * 6 - 1;
    if (i < 3) ssd1306_printFixed(curx - 5, cury, ".", STYLE_NORMAL);
  }

  for(;;) {
    ssd1306_drawBitmap(104, 6, 24, 16, spaceguy1);
    delay(100);
    ssd1306_drawBitmap(104, 6, 24, 16, spaceguy2);
	delay(100);
  }
}

void loop() {
  if (ether.packetReceive()) {
    byte ippad;
    if (ether.buffer[ETH_TYPE_H_P] == ETHTYPE_ARP_H_V &&
        ether.buffer[ETH_TYPE_L_P] == ETHTYPE_ARP_L_V) {
      ippad = ETH_ARP_SRC_IP_P;
    } else ippad = IP_SRC_P;
    displayMAC((byte *) ether.buffer + ETH_SRC_MAC, ether.buffer + ippad);
  } else {
    ether.clientIcmpRequest(broadcast);
    delay(300);
  }
}
