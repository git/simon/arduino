Regular Arduino firmware has a boot delay of over 1 second.

This directory contains a copy of `optiboot` for the atmega328p used in the Arduino Pro Mini.

First, if you don't already have one, create an ISP using a spare Arduino, following [these steps](https://www.arduino.cc/en/pmwiki.php?n=Tutorial/ArduinoISP).

Once complete, flash this `optiboot` firmware using the below command (on Unix, with `avrdude` in your `PATH`):

```
avrdude -v -v -c arduino -p atmega328p -P /dev/ttyUSB0 -b 19200 -U "flash:w:./optiboot_flash_atmega328p_UART0_57600_16000000L_B5.hex"
```

Or simply use the `Makefile` provided in this directory to execute the above command (again, on UNIX):

```
make bootloader
```

The bootloader was compiled using [this repository](https://github.com/MCUdude/optiboot_flash).

-----

For my own reference, the FTDI USB 6-pin pinout is:

- Red wire： 5V
- Black wire： GND
- White wire： RXD (connect to TX)
- Green wire： TXD (connect to RX)
- Yellow wire： RTS (connect to RST for code upload, disconnect to burn bootloader)
- Blue wire： CTS (disconnect)
