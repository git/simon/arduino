/* 
    MAC Address Reader - Arduino Nano + ssd1306 Version
    Copyright (c) 2018-2021 Simon Mollema

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

/* for a 128x32 size display using SPI to communicate */

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Fonts/FreeMono9pt7b.h>
#include <Adafruit_SSD1306.h>
#include <EtherCard.h>

byte Ethernet::buffer[800];
static byte mymac[] = { 0x12,0x34,0x56,0x78,0x90,0xab };
static byte myip[] = { 192,168,0,42 };
static byte broadcast[] = { 0xff,0xff,0xff,0xff,0xff,0xff };
byte ethpin = 9;

#define OLED_RESET  6
#define OLED_DC     7
#define OLED_CS     8
Adafruit_SSD1306 display(OLED_DC, OLED_RESET, OLED_CS);

void displayMAC(byte octets[6], byte ipoct[4]);
void drawError(char* error);

void setup() {
  display.begin(SSD1306_SWITCHCAPVCC);

  display.clearDisplay();
  display.setTextColor(WHITE);
  display.setFont(&FreeMono9pt7b);
  display.setCursor(0, 10);
  display.println("MAC WAITING");
  display.println("~ Simon M ~");
  display.display();

  if (ether.begin(sizeof Ethernet::buffer, mymac, ethpin) == 0) {
    drawError("MAC NO ETH ");
    display.display();
    delay(5000);
    displayMAC(mymac, myip);
    while(1);
  }
  
  ether.staticSetup(myip, broadcast);
  ether.enablePromiscuous();
  ether.enableBroadcast();
}

void drawError(char* error) {
  display.clearDisplay();
  display.setCursor(0, 10);
  display.print(error);
}

void displayMAC(byte octets[6], byte ipoct[4]) {
  if (!octets) {
    drawError("MAC ERRNULL");
    display.display();
    return;
  }
  display.clearDisplay();
  display.setCursor(0, 10);
  display.print("MAC");

  char octet[5];
  // MAC
  display.setCursor(46, 10);
  byte cury = 10;
  for (byte i=0;i<6;++i) {
    if (i == 3) {
      display.setCursor(46, 30);
      cury = 30;
    }
    sprintf(octet, "%02x", octets[i]);
    display.print(octet);
    if ((i+1) % 3) {
      display.setCursor(display.getCursorX()-1, cury);
      display.write(':');
      display.setCursor(display.getCursorX()-2, cury);
    }
  }
  // IP (small font)
  display.setFont();
  display.setCursor(0, 14);
  for (byte i=0;i<4;++i) {
    if (i == 2) display.setCursor(0, 25);
    char sep = (i+1) % 2 ? '.' : 0;
    sprintf(octet, "%d%c", ipoct[i], sep);
    display.print(octet);
  }

  // reset font
  display.setFont(&FreeMono9pt7b);
  display.display();
}

static byte isARP() {
    return ether.buffer[ETH_TYPE_H_P] == ETHTYPE_ARP_H_V &&
           ether.buffer[ETH_TYPE_L_P] == ETHTYPE_ARP_L_V;
}
static byte done = 0;
void loop() {
  if (done || ether.packetReceive()) {
    done = 1;
	byte *ipaddy = ether.buffer + (isARP() ? ETH_ARP_SRC_IP_P : IP_SRC_P);
    displayMAC((byte *) ether.buffer + ETH_SRC_MAC, ipaddy);
    delay(65535);
  } else {
    ether.clientIcmpRequest(broadcast);
    delay(300);
  }
}
